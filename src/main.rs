extern crate rand;
extern crate time;

mod sorts;

use rand::Rng;
use time::PreciseTime;
use std::fmt::Debug;
use std::ops::Add;
use std::ops::Div;

use sorts::sorts::{mergesort, quicksort, insertsort, heapsort};

trait Sortable<T: Copy + Ord + Debug> {
    fn qsort<F>(&mut self, cmp: &F) where F: Fn(&T, &T) -> bool;
    fn mergesort<F>(&mut self, cmp: &F) where F: Fn(&T, &T) -> bool;
    fn shuffle(&mut self, rnd: &mut rand::ThreadRng);
    fn is_sorted(&self) -> bool;
}

impl <T: Copy + Ord + Debug> Sortable<T> for [T] {
    fn qsort<F>(&mut self, cmp: &F)
        where F: Fn(&T, &T) -> bool
    {
        quicksort::qsort1(self, cmp);
    }

    fn mergesort<F>(&mut self, cmp: &F) where F: Fn(&T, &T) -> bool
    {
        mergesort::merge_sort1(self, cmp);
    }

    fn shuffle(&mut self, rnd: &mut rand::ThreadRng) {
        for i in 0..self.len()-1 {
            let ind = rnd.next_u64() % self.len() as u64;
            self.swap(i, ind as usize);
        }
    }

    fn is_sorted(&self) -> bool {
        for i in 0..self.len()-1 {
            if self[i] > self[i+1] {
                println!("ARRAY NOT SORTED!!! ind: {}, {} = {:?}, {:?}", i, i+1, self[i], self[i+1]);
                return false;
            }
        }

        true
    }
}

fn bench<Q: Copy + Ord + Debug>(name: &str, arr: &Vec<Q>, f: &mut FnMut(&mut [Q])) {
    let mut av_time: time::Duration = time::Duration::zero();
    let count = 10;

    for i in 0..count {
        let mut local_arr = arr.clone();

        let start = PreciseTime::now();
        f(&mut local_arr);
        let end = PreciseTime::now();

//        local_arr.is_sorted();

        av_time = av_time.add(start.to(end));
    }

    av_time = av_time.div(count);

    println!("{}: {}", name, av_time);
}

fn test() {
    let mut rnd = rand::thread_rng();
    let mut arr: Vec<i32> = Vec::new();

    let count = 10;
    for _ in 0..count {
        let val = (rnd.next_u32() % 2_000) as i32 - 1_000;
        arr.push(val);
    }

    println!("arr: {:?}", arr);

    sorts::sorts::heapsort::heap_sort1(&mut arr, &|x, y| x < y);
//    sorts::sorts::insert_sort1(&mut arr, &|x, y| x < y);
//    sorts::sorts::merge_sort1(&mut arr, &|x, y| x < y);

    println!("arr: {:?}", arr);
}


fn main() {
    let mut rnd = rand::thread_rng();
    let mut arr: Vec<i32> = Vec::new();

    let count = 10_000_000;
    for i in 0..count {
        let val = (rnd.next_u32() % 2_000_000) as i32 - 1_000_000;
        arr.push(val);
//        arr.push(count-i);
    }

//    test();
//    return;

    bench("qsort", &mut arr, &mut |arr| {
        quicksort::qsort1(arr, &|x, y| x < y);
    });

    bench("qsort2", &mut arr, &mut |arr| {
        quicksort::qsort2(arr, &|x, y| x < y);
    });

    bench("mergesort1", &mut arr, &mut |arr| {
        mergesort::merge_sort1(arr, &|x, y| x < y);
    });

    bench("mergesort2", &mut arr, &mut |arr| {
        mergesort::merge_sort2(arr);
    });

    bench("mergesort3", &mut arr, &mut |arr| {
        mergesort::merge_sort3(arr, &|x, y| x < y);
    });

    bench("heapsort1", &arr, &mut |arr| {
        heapsort::heap_sort1(arr, &|x, y| x < y);
    });

    bench("sort", &mut arr, &mut |arr| arr.sort());

}
