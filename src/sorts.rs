
pub mod sorts {

    pub mod quicksort {
        use sorts::sorts::heapsort;

        pub fn qsort1<T, F>(arr: &mut [T], cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let mut len = arr.len();

            if len >= 2 {
                len -= 1;

                let mut store_ind: usize = 0;

                arr.swap(len / 2, len);

                for i in 0..len {
                    if cmp(&arr[i], &arr[len]) {
                        arr.swap(i, store_ind);
                        store_ind += 1;
                    }
                }

                arr.swap(store_ind, len);

                qsort1(&mut arr[..store_ind], cmp);
                qsort1(&mut arr[store_ind + 1..], cmp);
            }
        }

        pub fn qsort2<T, F>(arr: &mut [T], cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let mut len = arr.len();

            if len > 20 {
                len -= 1;

                let mut store_ind: usize = 0;

                arr.swap(len / 2, len);

                for i in 0..len {
                    if cmp(&arr[i], &arr[len]) {
                        arr.swap(i, store_ind);
                        store_ind += 1;
                    }
                }

                arr.swap(store_ind, len);

                qsort1(&mut arr[..store_ind], cmp);
                qsort1(&mut arr[store_ind + 1..], cmp);
            } else {
                heapsort::heap_sort1(&mut arr[..], cmp);
            }
        }
    }

    pub mod mergesort {
        use sorts::sorts::heapsort;

        pub fn merge_sort1<T: Copy, F>(arr: &mut [T], cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let len = arr.len();
            let mut temp: Vec<T> = Vec::with_capacity(len);
            temp.extend_from_slice(arr);
            merge_sort_impl1(arr, &mut temp, 0, len, cmp);
        }

        fn merge_sort_impl1<T: Copy, F>(arr: &mut [T], temp: &mut [T], start: usize, end: usize, cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let size = end - start;

            if size >= 2 {
                let mid = (start + end) / 2;

                merge_sort_impl1(arr, temp, start, mid, cmp);
                merge_sort_impl1(arr, temp, mid, end, cmp);

                // merge
                let mut s = start;
                let mut e = mid;
                for i in start..end {
                    if s < mid && (e >= end || cmp(&arr[s], &arr[e])) {
                        temp[i] = arr[s];
                        s += 1;
                    } else {
                        temp[i] = arr[e];
                        e += 1;
                    }
                }

                // copy
                for i in start..end {
                    arr[i] = temp[i];
                }
            }
        }

        pub fn merge_sort3<T: Copy, F>(arr: &mut [T], cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let len = arr.len();
            let mut temp: Vec<T> = Vec::with_capacity(len);
            temp.extend_from_slice(arr);
            merge_sort_impl3(arr, &mut temp, 0, len, cmp);
        }

        fn merge_sort_impl3<T: Copy, F>(arr: &mut [T], temp: &mut [T], start: usize, end: usize, cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let size = end - start;

            if size > 20 {
                let mid = (start + end) / 2;

                merge_sort_impl1(arr, temp, start, mid, cmp);
                merge_sort_impl1(arr, temp, mid, end, cmp);

                // merge
                let mut s = start;
                let mut e = mid;
                for i in start..end {
                    if s < mid && (e >= end || cmp(&arr[s], &arr[e])) {
                        temp[i] = arr[s];
                        s += 1;
                    } else {
                        temp[i] = arr[e];
                        e += 1;
                    }
                }

                // copy
                for i in start..end {
                    arr[i] = temp[i];
                }
            } else {
                heapsort::heap_sort1(&mut arr[..], cmp);
            }
        }

        pub fn merge_sort2<T: Copy + Ord>(arr: &mut [T]) {
            let len = arr.len();

            if len > 1 {
                merge_sort2(&mut arr[..len/2]);
                merge_sort2(&mut arr[len/2..]);
                merge_impl2(arr);
            }
        }

        fn merge_impl2<T: Copy + Ord>(arr: &mut [T]) {
            let len = arr.len();
            let mid = len / 2;
            let mut temp: Vec<T> = Vec::with_capacity(len);

            let mut s = 0;
            let mut e = mid;

            for _ in 0..len {
                if s < mid && (e >= len || arr[s] < arr[e]) {
                    temp.push(arr[s]);
                    s += 1;
                } else {
                    temp.push(arr[e]);
                    e += 1;
                }
            }

            arr.copy_from_slice(&temp[..]);
        }
    }

    pub mod insertsort {
        pub fn insert_sort1<T, F>(arr: &mut [T], cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            for i in 0..arr.len() {
                for j in (1..i + 1).rev() {
                    if !cmp(&arr[j - 1], &arr[j]) {
                        arr.swap(j - 1, j);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    pub mod heapsort {
        pub fn heap_sort1<T, F>(arr: &mut [T], cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let len = arr.len();

            for i in (0..len/2).rev() {
                sift_down1(arr, i, len-1, cmp);
            }

            for i in (1..len).rev() {
                arr.swap(0, i);
                sift_down1(arr, 0, i-1, cmp);
            }
        }

        fn sift_down1<T, F>(arr: &mut [T], start: usize, end: usize, cmp: &F)
            where F: Fn(&T, &T) -> bool
        {
            let mut root = start;

            loop {
                let mut child = root * 2 + 1;

                if child > end {
                    break;
                }

                if child + 1 <= end && cmp(&arr[child], &arr[child+1]) {
                    child += 1;
                }

                if cmp(&arr[root], &arr[child]) {
                    arr.swap(root, child);
                    root = child;
                } else {
                    break;
                }
            }
        }
    }
}